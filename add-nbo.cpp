#include <stddef.h> // for size_t
#include <stdint.h> // for uint8_t
#include <stdio.h> // for printf
#include <netinet/in.h>// for ntohl


int main(int argc, char** argv) {
    if (argc != 3)
    {
        printf("input error\n");
        return -1;
    }
    int i = 1;
    FILE* fp;       //file pointer
    uint32_t augend = 0, adder;     //added value, adding value
    while (i!=argc)
    {
        fp = fopen(argv[i], "rb");
        if (fp == NULL)
            printf("filepointer error\n");
        fread(&adder, 1, 4, fp); //ntohl needed
        adder = ntohl(adder);
        augend += adder;
        printf("%d(0x%x) ", adder, adder);
        if (i == 1)
            printf("+ ");
        else
            printf("= ");
        i++;
    }
    printf("%d(0x%x) ", augend, augend);
    return 0;
}
